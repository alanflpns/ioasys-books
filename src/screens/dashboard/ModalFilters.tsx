import React, {memo, useState} from 'react';
import {Modal, ScrollView} from 'react-native';

import {
  ModalBackground,
  ModalContainer,
  ModalHeader,
  IconModal,
  FiltersContainer,
  FiltersTitle,
  FiltersButtons,
  FiltersButton,
  FilterBtn,
} from './styles';
import theme from '../../global/styles/theme';
import {YEARS, CATEGORIES} from '../../Constants';
import {useBook} from '../../hooks/useBook';

interface IModalFiltersProps {
  isVisible: boolean;
  closeModal(): void;
}

function ModalFiltersComponent({isVisible, closeModal}: IModalFiltersProps) {
  const {getBookList} = useBook();

  const [categories, setCategories] = useState<string[]>([]);
  const [years, setYears] = useState<number[]>([]);

  function submitFilters() {
    const page = 1;
    if (categories.length > 0 && years.length > 0) {
      getBookList(page, {category: categories, published: years});
    } else if (categories.length > 0) {
      getBookList(page, {category: categories});
    } else if (years.length > 0) {
      getBookList(page, {published: years});
    } else {
      getBookList(page);
    }

    closeModal();
  }

  function changeCategory(key: string) {
    let newCategories;

    if (categories.includes(key)) {
      newCategories = categories.filter(cat => cat !== key);
    } else {
      newCategories = [...categories, key];
    }

    setCategories(newCategories);
  }

  function changeYear(key: number) {
    let newYears;

    if (years.includes(key)) {
      newYears = years.filter(year => year !== key);
    } else {
      newYears = [...years, key];
    }

    setYears(newYears);
  }

  return (
    <Modal
      visible={isVisible}
      animationType="fade"
      transparent
      statusBarTranslucent>
      <ModalBackground>
        <ModalContainer>
          <ModalHeader onPress={() => closeModal()}>
            <IconModal name="close" size={20} color={theme.colors.black} />
          </ModalHeader>

          <ScrollView showsVerticalScrollIndicator={false}>
            <FiltersContainer>
              <FiltersTitle>Selecione a categoria</FiltersTitle>
              <FiltersButtons>
                {CATEGORIES.map(category => (
                  <FiltersButton
                    onPress={() => changeCategory(category.key)}
                    type={categories.includes(category.key) ? 'black' : 'light'}
                    key={category.key}>
                    {category.name}
                  </FiltersButton>
                ))}
              </FiltersButtons>
            </FiltersContainer>

            <FiltersContainer>
              <FiltersTitle>Selecione o ano</FiltersTitle>
              <FiltersButtons>
                {YEARS.map(year => (
                  <FiltersButton
                    onPress={() => changeYear(year)}
                    type={years.includes(year) ? 'black' : 'light'}
                    key={year}>
                    {year}
                  </FiltersButton>
                ))}
              </FiltersButtons>
            </FiltersContainer>
          </ScrollView>

          <FilterBtn onPress={() => submitFilters()} type="default">
            Filtrar
          </FilterBtn>
        </ModalContainer>
      </ModalBackground>
    </Modal>
  );
}

export const ModalFilters = memo(ModalFiltersComponent);
