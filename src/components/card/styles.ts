import styled from 'styled-components/native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

export const Container = styled.TouchableOpacity`
  background-color: ${({theme}) => theme.colors.white};
  padding: 20px;
  margin-bottom: 16px;
  border-radius: 5px;

  shadow-color: black;
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 1.41px;

  elevation: 2;
`;

export const BookContainer = styled.View`
  flex-direction: row;
`;

export const ImageBook = styled.Image`
  height: ${RFValue(181)}px;
  width: ${RFValue(122)}px;
  margin-right: 20px;
`;

export const InfoContainer = styled.View`
  justify-content: space-between;
`;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.bold};
  color: ${({theme}) => theme.colors.black};
  font-size: ${RFValue(14)}px;
  line-height: 20px;
  max-width: ${RFPercentage(20)}px;
`;

export const Author = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.pink800};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
  max-width: ${RFPercentage(20)}px;
`;

export const Pages = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.silver700};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
`;

export const Company = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.silver700};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
  max-width: ${RFPercentage(20)}px;
`;

export const Publication = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.silver700};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
`;
