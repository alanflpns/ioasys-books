# iOasys Books

## 🚀 Objetivo

- Aplicativo de listagem de livros criado com o objetivo de completar o desafio ReactNative para empresa iOasys,

## 🚀 Bibliotecas

- [axios](https://github.com/axios/axios) para requisições da API.
- [react-navigation](https://reactnavigation.org/) biblioteca de navegação entre telas.
- [lottie](https://github.com/lottie-react-native/lottie-react-native) para animação do carregamento dos livros
- [react-hook-form](https://react-hook-form.com/) usado para criação dos inputs, aumentando assim a performance ao invés de utilizar estados do react.
- [react-native-iphone-x-helper](https://github.com/ptelad/react-native-iphone-x-helper) utilizado para pegar o tamanho do status bar nos iPhones recentes.
- [react-native-responsive-fontsize](https://www.npmjs.com/package/react-native-responsive-fontsize) utilizado para poder usar as fontes de tamanhos relativos a proporção do celular do usuário
- [react-native-splash-screen](https://github.com/crazycodeboy/react-native-splash-screen) utilizado para controle da splash screen
- [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) biblioteca de icones
- [styled-components](https://styled-components.com/) usado para estilização dos componentes

## ✅ Pré-requisitos

- [Node.js > 12](https://nodejs.org)
- [Yarn](https://yarnpkg.com/)
- [Xcode](https://developer.apple.com/xcode)
- [Watchman](https://facebook.github.io/watchman)
- [JDK > 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Cocoapods](https://cocoapods.org)
- [Android Studio and Android SDK](https://developer.android.com/studio)

## 🏃 Como usar

```bash
# Clone o repositório
git clone https://alanflpns@bitbucket.org/alanflpns/ioasys-books.git

# Navegue até a pasta
cd ioasys-books

# Instale as dependências
yarn install

# Instale as dependencias do IOS
cd ios && pod install

# Para executar no IPhone
yarn ios

# Para executar no Android
yarn android
```

## 📱 Gerar aplicativo

- Gerando aplicativo no Android(APK)

```bash
# Execute o comando
yarn build-android
```

- Gerando aplicativo no iPhone(IPA)

```bash
- Abrir XCode
- Product -> Archive

```

Para mais informações -> https://reactnative.dev/docs/publishing-to-app-store
