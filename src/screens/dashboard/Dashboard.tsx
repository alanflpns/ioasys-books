import React, {useEffect} from 'react';
import {StatusBar, Alert, ActivityIndicator} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {HeaderTitle} from '../../components/headerTitle/HeaderTitle';
import {
  Logout,
  Icon,
  Container,
  ContainerLoading,
  ContainerHeader,
} from './styles';
import {useAuth} from '../../hooks/useAuth';
import {useBook} from '../../hooks/useBook';
import theme from '../../global/styles/theme';
import {BookList} from './BookList';
import {SearchBook} from './SearchBook';
import {LoadingAnimation} from '../../components/loadingAnimation/LoadingAnimation';

export function Dashboard() {
  const {logout} = useAuth();
  const {getBookList, bookList, isLoadingBookList, getMoreBooks, getInfoBook} =
    useBook();
  const navigation = useNavigation();

  useEffect(() => {
    const page = 1;

    getBookList(page);
  }, []);

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />

      <ContainerHeader>
        <HeaderTitle type="dark" />
        <Logout
          activeOpacity={0.5}
          onPress={() =>
            Alert.alert('Saindo...', 'Tem certeza que deseja sair?', [
              {text: 'Não'},
              {text: 'Sim', onPress: () => logout()},
            ])
          }>
          <Icon name="logout" />
        </Logout>
      </ContainerHeader>

      <Container>
        <SearchBook />

        {isLoadingBookList ? <LoadingAnimation /> : <BookList />}
      </Container>
    </>
  );
}
