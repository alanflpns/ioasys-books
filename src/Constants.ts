export const API_URL = 'https://books.ioasys.com.br/api/v1';

export const KEY_USER_STORAGE = '@iOasys/user';

export const CATEGORIES = [
  {
    key: 'biographies',
    name: 'Biografias',
  },
  {
    key: 'collections',
    name: 'Coleções',
  },
  {
    key: 'behavior',
    name: 'Comportamento',
  },
  {
    key: 'tales',
    name: 'Contos',
  },
  {
    key: 'literary-criticism',
    name: 'Crítica literária',
  },
  {
    key: 'scienceFiction',
    name: 'Ficção científica',
  },
  {
    key: 'folklore',
    name: 'Folclore',
  },
  {
    key: 'genealogy',
    name: 'Genealogia',
  },
  {
    key: 'humor',
    name: 'Humor',
  },
  {
    key: 'children',
    name: 'Crianças',
  },
  {
    key: 'games',
    name: 'Jogos',
  },
  {
    key: 'newspapers',
    name: 'Jornais',
  },
  {
    key: 'brazilian-literature',
    name: 'Literatura brasileira',
  },
  {
    key: 'foreign-literature',
    name: 'Literatura estrangeira',
  },
  {
    key: 'rare-books',
    name: 'Livros raros',
  },
  {
    key: 'manuscripts',
    name: 'Manuscritos',
  },
  {
    key: 'poetry',
    name: 'Poesia',
  },
  {
    key: 'another-subjects',
    name: 'Outros assuntos',
  },
];

export const YEARS = [2013, 2014, 2015, 1016, 2017, 2018, 2019, 2020, 2021];
