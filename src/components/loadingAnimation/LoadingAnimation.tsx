import React from 'react';
import LottieView from 'lottie-react-native';

import {Container} from './styles';
import booksLoading from '../../assets/json/books-loading.json';

export function LoadingAnimation() {
  return (
    <Container>
      <LottieView source={booksLoading} autoPlay loop />
    </Container>
  );
}
