export default {
  colors: {
    white: '#FFFFFF',
    whiteTransparent: 'rgba(255,255,255,0.2)',
    black: '#000000',
    blackTransparent: 'rgba(0,0,0,0.32)',
    pink800: '#B22E6F',
    red200: '#ffb3b3',
    red500: '#ff0000',
    silver500: '#E5E5E5',
    silver700: '#999999',
  },
  fonts: {
    light: 'Heebo-Light',
    regular: 'Heebo-Regular',
    medium: 'Heebo-Medium',
    bold: 'Heebo-Bold',
  },
};
