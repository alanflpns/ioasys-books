import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface ITypeHeader {
  type: 'dark' | 'light';
}

export const Container = styled.View`
  flex-direction: row;
  align-self: flex-start;
`;

export const TextStrong = styled.Text<ITypeHeader>`
  font-family: ${({theme}) => theme.fonts.bold};
  color: ${({theme, type}) =>
    type === 'dark' ? theme.colors.black : theme.colors.white};
  font-size: ${RFValue(36)}px;
  margin-right: 16px;
`;

export const TextLight = styled.Text<ITypeHeader>`
  font-family: ${({theme}) => theme.fonts.light};
  color: ${({theme, type}) =>
    type === 'dark' ? theme.colors.black : theme.colors.white};
  font-size: ${RFValue(36)}px;
`;
