import React, {RefObject} from 'react';
import {TextInputProps, TextInput as TextInputComponent} from 'react-native';

import {
  Container,
  InputContainer,
  Info,
  Label,
  TextInput,
  Action,
  ErrorMessage,
} from './styles';
import theme from '../../global/styles/theme';

interface IInputProps extends Omit<TextInputProps, 'ref'> {
  type: 'dark' | 'light';
  label?: string;
  action?: React.ReactNode;
  inputRef?: RefObject<TextInputComponent>;
  errorMessage?: string;
}

export function Input({
  label,
  action,
  inputRef,
  errorMessage,
  type,
  ...rest
}: IInputProps) {
  return (
    <>
      <Container>
        <InputContainer type={type} hasError={!!errorMessage}>
          <Info>
            {label && <Label>{label}</Label>}
            <TextInput
              type={type}
              selectionColor={
                type === 'light' ? theme.colors.black : theme.colors.white
              }
              placeholderTextColor={
                type === 'light'
                  ? theme.colors.silver700
                  : theme.colors.whiteTransparent
              }
              ref={inputRef}
              {...rest}
            />
          </Info>
          {action && <Action>{action}</Action>}
        </InputContainer>
      </Container>
      {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </>
  );
}
