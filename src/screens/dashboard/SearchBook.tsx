import React, {useState, useRef} from 'react';

import {TextInput} from 'react-native';
import {Input} from '../../components/input/Input';
import {
  SearchContainer,
  ContainerInput,
  Filter,
  FilterIcon,
  ButtonSearch,
  SearchIcon,
} from './styles';
import theme from '../../global/styles/theme';
import {useBook} from '../../hooks/useBook';
import {ModalFilters} from './ModalFilters';

export function SearchBook() {
  const {getBookList} = useBook();
  const [search, setSearch] = useState('');

  const [showModal, setShowModal] = useState(false);

  const inputRef = useRef<TextInput>(null);

  function searchBook() {
    const page = 1;

    if (search.trim()) {
      getBookList(page, {title: search});
    } else {
      getBookList(page);
    }

    inputRef.current?.blur();
  }

  return (
    <>
      <SearchContainer>
        <ContainerInput style={{flex: 1}}>
          <Input
            type="light"
            placeholder="Procure um livro"
            onChangeText={setSearch}
            onEndEditing={() => searchBook()}
            inputRef={inputRef}
            action={
              <ButtonSearch onPress={() => searchBook()}>
                <SearchIcon
                  name="search"
                  size={35}
                  color={theme.colors.black}
                />
              </ButtonSearch>
            }
          />
        </ContainerInput>
        <Filter onPress={() => setShowModal(!showModal)}>
          <FilterIcon name="sliders" size={30} color={theme.colors.black} />
        </Filter>
      </SearchContainer>

      <ModalFilters
        isVisible={showModal}
        closeModal={() => setShowModal(!showModal)}
      />
    </>
  );
}
