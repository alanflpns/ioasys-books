import React from 'react';
import {Container, TextStrong, TextLight} from './styles';

interface IHeaderTitleProps {
  type: 'dark' | 'light';
}

export function HeaderTitle({type}: IHeaderTitleProps) {
  return (
    <Container>
      <TextStrong type={type}>ioasys</TextStrong>
      <TextLight type={type}>Books</TextLight>
    </Container>
  );
}
