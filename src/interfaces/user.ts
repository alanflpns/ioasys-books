export interface ILogin {
  email: string;
  password: string;
}

export interface IUser {
  id: string;
  name: string;
  birthdate: string;
  gender: string;
  token: string;
}
