import axios from 'axios';

import {API_URL} from '../Constants';

const AxiosApi = axios;
AxiosApi.defaults.baseURL = API_URL;

export const setBearerToken = (token: string): void => {
  AxiosApi.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export default AxiosApi;
