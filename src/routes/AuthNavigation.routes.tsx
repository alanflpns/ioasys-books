import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Login} from '../screens/login/Login';

export function AuthNavigation() {
  const Auth = createNativeStackNavigator();

  return (
    <Auth.Navigator screenOptions={{headerShown: false}}>
      <Auth.Screen name="Login" component={Login} />
    </Auth.Navigator>
  );
}
