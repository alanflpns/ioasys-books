import React, {useMemo} from 'react';
import {TouchableOpacityProps, ActivityIndicator} from 'react-native';

import {Container, TextButton, LoadingContainer} from './styles';
import theme from '../../global/styles/theme';

interface IButtonProps extends TouchableOpacityProps {
  type: 'light' | 'black' | 'default';
  isLoading?: boolean;
}

export function Button({isLoading, type, ...rest}: IButtonProps) {
  const loadingColor = useMemo(
    () =>
      type === 'light'
        ? theme.colors.black
        : type === 'black'
        ? theme.colors.white
        : theme.colors.pink800,
    [],
  );

  return (
    <Container type={type} activeOpacity={0.6} disabled={isLoading} {...rest}>
      <TextButton type={type} isLoading={isLoading}>
        {rest.children}
      </TextButton>
      {isLoading && (
        <LoadingContainer>
          <ActivityIndicator color={loadingColor} />
        </LoadingContainer>
      )}
    </Container>
  );
}
