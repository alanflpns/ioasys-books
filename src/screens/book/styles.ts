import styled from 'styled-components/native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {RFValue} from 'react-native-responsive-fontsize';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

export const ContainerHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: ${`${getStatusBarHeight() + 20}px`} 16px 20px 16px;
  align-items: center;
  background-color: ${({theme}) => theme.colors.white};
`;

export const BackScreen = styled.TouchableOpacity`
  padding: 10px;
  border-width: 1px;
  border-radius: 100px;
  border-color: ${({theme}) => theme.colors.blackTransparent};
`;

export const Icon = styled(MaterialIcon)`
  font-size: ${RFValue(20)}px;
  color: ${({theme}) => theme.colors.black};
`;

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.white};
  padding: 0 40px;
  padding-bottom: 20px;
`;

export const ContainerLoading = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const BookImage = styled.Image`
  width: 100%;
  height: ${RFValue(415)}px;
  margin-bottom: 24px;
`;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.bold};
  color: ${({theme}) => theme.colors.black};
  font-size: ${RFValue(28)}px;
  line-height: 40px;
`;

export const Authors = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.pink800};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
  margin-bottom: 32px;
`;

export const TitleInfo = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.black};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
`;

export const InfoContainer = styled.View`
  margin-top: 15px;
  margin-bottom: 16px;
`;

export const Info = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Label = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.black};
  font-size: ${RFValue(12)}px;
  line-height: 28px;
`;

export const Value = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.silver700};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
`;

export const TitleResenha = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.black};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
  margin-bottom: 15px;
`;

export const Resenha = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.silver700};
  font-size: ${RFValue(12)}px;
  line-height: 20px;
`;
