import AxiosApi from './AxiosApi';
import {IResponseRequest} from '../interfaces/system';
import {IUser} from '../interfaces/user';
import {IBookResponseRequest, IBook} from '../interfaces/book';
import {stringifyQueryStringArray} from '../utils/fnUtils';
import {IFilters} from '../contexts/BookContext';

export default {
  auth: {
    login: (email: string, password: string) => {
      const request: Promise<IResponseRequest<IUser>> = AxiosApi.post(
        `/auth/sign-in`,
        {email, password},
      );

      return request;
    },
  },
  books: {
    getBookList: (page: number, filters?: IFilters) => {
      const request: Promise<IResponseRequest<IBookResponseRequest>> =
        AxiosApi.get(`/books?${stringifyQueryStringArray({page, ...filters})}`);

      return request;
    },
    getBookById: (bookId: string) => {
      const request: Promise<IResponseRequest<IBook>> = AxiosApi.get(
        `/books/${bookId}`,
      );

      return request;
    },
  },
};
