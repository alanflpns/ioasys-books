import React, {useRef} from 'react';
import {
  StatusBar,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';

import {useForm, Controller, SubmitHandler} from 'react-hook-form';

import {ImgBackground, Container, InputContainer} from './styles';
import image from '../../global/image';
import {HeaderTitle} from '../../components/headerTitle/HeaderTitle';
import {Input} from '../../components/input/Input';
import {Button} from '../../components/button/Button';
import {validateEmail} from '../../utils/validates';
import {useAuth} from '../../hooks/useAuth';

interface ILoginForm {
  email: string;
  password: string;
}

export function Login() {
  const {requestLogin, isLoading} = useAuth();
  const passwordInputRef = useRef<TextInput>(null);

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm<ILoginForm>();

  const submitForm: SubmitHandler<ILoginForm> = async data => {
    requestLogin(data);
  };

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />

      <ImgBackground source={image.backgroundLogin} />

      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flexGrow: 1}}>
          <Container>
            <HeaderTitle type="light" />

            <InputContainer>
              <Controller
                name="email"
                control={control}
                rules={{required: 'Informe um email', validate: validateEmail}}
                render={({field: {onChange, value}}) => (
                  <Input
                    type="dark"
                    label="Email"
                    returnKeyType="next"
                    keyboardType="email-address"
                    placeholder="Insira o email"
                    autoCapitalize="none"
                    onSubmitEditing={() => passwordInputRef.current?.focus()}
                    onChangeText={onChange}
                    value={value}
                    errorMessage={errors.email?.message}
                  />
                )}
              />

              <Controller
                name="password"
                control={control}
                rules={{required: 'Informe a senha'}}
                render={({field: {onChange, value}}) => (
                  <Input
                    type="dark"
                    label="Senha"
                    secureTextEntry
                    returnKeyType="send"
                    placeholder="Insira a senha"
                    inputRef={passwordInputRef}
                    onChangeText={onChange}
                    value={value}
                    action={
                      <Button
                        type="default"
                        isLoading={isLoading}
                        onPress={handleSubmit(submitForm)}>
                        Entrar
                      </Button>
                    }
                    onSubmitEditing={handleSubmit(submitForm)}
                    errorMessage={errors.password?.message}
                  />
                )}
              />
            </InputContainer>
          </Container>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}
