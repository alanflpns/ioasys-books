import styled from 'styled-components/native';

export const ImgBackground = styled.Image`
  width: 135%;
  height: 100%;
  position: absolute;
`;

export const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;

  padding: 0 20px;
`;

export const InputContainer = styled.View`
  margin-top: 48px;
`;
