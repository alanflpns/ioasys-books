import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SlashScreen from 'react-native-splash-screen';

import {AuthNavigation} from './routes/AuthNavigation.routes';
import {useAuth} from './hooks/useAuth';
import {KEY_USER_STORAGE} from './Constants';
import {setBearerToken} from './services/AxiosApi';
import {IUser} from './interfaces/user';
import {AppNavigation} from './routes/AppNavigation.routes';
import {BookContextProvider} from './contexts/BookContext';

export function AppInitialSetup() {
  const {user, setUser} = useAuth();

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getStorage();
  }, []);

  useEffect(() => {
    if (user) {
      setIsLoading(false);
    }
  }, [user]);

  useEffect(() => {
    if (!isLoading) {
      SlashScreen.hide();
    }
  }, [isLoading, user]);

  async function getStorage() {
    const userStorage = await AsyncStorage.getItem(KEY_USER_STORAGE);

    if (userStorage) {
      const newUser: IUser = JSON.parse(userStorage);

      setBearerToken(newUser.token);
      setUser(newUser);
    } else {
      setIsLoading(false);
    }
  }

  return user ? (
    <BookContextProvider>
      <AppNavigation />
    </BookContextProvider>
  ) : (
    <AuthNavigation />
  );
}
