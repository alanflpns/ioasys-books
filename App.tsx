import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {ThemeProvider} from 'styled-components';

import theme from './src/global/styles/theme';
import {AuthContextProvider} from './src/contexts/AuthContext';
import {AppInitialSetup} from './src/AppInitialSetup';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <NavigationContainer>
        <AuthContextProvider>
          <AppInitialSetup />
        </AuthContextProvider>
      </NavigationContainer>
    </ThemeProvider>
  );
}

export default App;
