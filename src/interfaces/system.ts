import {AxiosResponse, AxiosError} from 'axios';

interface IResponseHeaders {
  authorization: string;
}

interface IError {
  message: string;
}

export interface IResponseRequest<T = unknown>
  extends Omit<AxiosResponse, 'data' | 'headers'> {
  data?: T;
  errors?: IError;
  headers: IResponseHeaders;
}

export interface IRefreshToken {
  refreshToken: string;
}

export interface IErrorResponse extends AxiosError<{errors: IError}> {}
