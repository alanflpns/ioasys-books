import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface TypeProps {
  type: 'dark' | 'light';
}
interface InputContainerProps extends TypeProps {
  hasError: boolean;
}

export const Container = styled.View`
  flex-direction: row;
  align-self: flex-start;
`;

export const InputContainer = styled.View<InputContainerProps>`
  background-color: ${({theme, type}) =>
    type === 'light' ? 'transparent' : theme.colors.blackTransparent};
  flex: 1;
  flex-direction: row;
  align-items: center;

  margin-bottom: ${({hasError}) => (hasError ? '0' : '20px')};
  border-radius: 5px;
  padding: 8px 13px;

  ${({hasError}) =>
    hasError &&
    css`
      border-width: 1px;
      border-color: red;
    `}

  ${({type, theme}) =>
    type === 'light' &&
    css`
      border-width: 1px;
      border-color: ${theme.colors.silver700};
    `}
`;

export const Info = styled.View`
  width: 100%;
  flex: 1;
`;

export const Label = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.white};
  font-size: ${RFValue(12)}px;

  opacity: 0.5;
`;

export const TextInput = styled.TextInput<TypeProps>`
  padding: ${({type}) =>
    type === 'light'
      ? `${RFValue(6)}px 0px ${RFValue(6)}px ${RFValue(2)}px`
      : ' 0px'};

  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme, type}) =>
    type === 'light' ? theme.colors.black : theme.colors.white};
  font-size: ${RFValue(16)}px;
`;

export const Action = styled.View`
  margin-left: 10px;
`;

export const ErrorMessage = styled.Text`
  margin-bottom: 4px;
  color: ${({theme}) => theme.colors.red200};
  font-family: ${({theme}) => theme.fonts.light};
`;
