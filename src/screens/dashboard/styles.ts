import styled from 'styled-components/native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';
import {FlatList} from 'react-native';
import {getBottomSpace, getStatusBarHeight} from 'react-native-iphone-x-helper';

import {IBook} from '../../interfaces/book';
import {Button} from '../../components/button/Button';

export const ContainerHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: ${`${getStatusBarHeight() + 20}px`} 16px 20px 16px;
  align-items: center;
  background-color: ${({theme}) => theme.colors.silver500};
`;

export const Logout = styled.TouchableOpacity`
  padding: 10px;
  border-width: 1px;
  border-radius: 100px;
  border-color: ${({theme}) => theme.colors.blackTransparent};
`;

export const Icon = styled(MaterialIcon)`
  font-size: ${RFValue(20)}px;
  color: ${({theme}) => theme.colors.black};
`;

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.silver500};
  padding: 0 16px;
`;

export const BooksList = styled(FlatList as new () => FlatList<IBook>).attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
    paddingBottom: getBottomSpace() + RFValue(10),
  },
})``;

export const NoBooksText = styled.Text`
  color: ${({theme}) => theme.colors.black};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
`;

export const ContainerLoading = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const SearchContainer = styled.View`
  flex-direction: row;
`;

export const ContainerInput = styled.View`
  flex: 1;
`;

export const ButtonSearch = styled.TouchableOpacity`
  margin: -10px -12px -10px 0;
  padding: 10px;
`;

export const SearchIcon = styled(EvilIcons)``;

export const Filter = styled.TouchableOpacity`
  padding: 15px 0 15px 25px;
`;

export const FilterIcon = styled(FontAwesome)``;

export const ModalBackground = styled.View`
  background-color: ${({theme}) => theme.colors.blackTransparent};
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const ModalContainer = styled.View`
  background: ${({theme}) => theme.colors.white};
  margin: 16px;
  border-radius: 5px;
  padding: 16px;
  align-items: center;

  max-height: ${RFPercentage(90)}px;
`;

export const ModalHeader = styled.TouchableOpacity`
  align-self: flex-end;
  border-width: 1px;
  border-color: ${({theme}) => theme.colors.silver700};
  border-radius: 50px;
  padding: 10px;
`;

export const IconModal = styled(MaterialIcon)``;

export const FiltersContainer = styled.View`
  margin-bottom: 40px;
`;

export const FiltersTitle = styled.Text`
  color: ${({theme}) => theme.colors.black};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
`;

export const FiltersButtons = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

export const FiltersButton = styled(Button)`
  margin-right: 5px;
  margin-top: 5px;
`;

export const FilterBtn = styled(Button)`
  border-width: 1px;
  border-color: ${({theme}) => theme.colors.pink800};
`;
