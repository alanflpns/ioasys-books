import React, {createContext, ReactNode, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {IUser, ILogin} from '../interfaces/user';
import {IErrorResponse} from '../interfaces/system';
import Requests from '../services/Requests';
import {KEY_USER_STORAGE} from '../Constants';
import {setBearerToken} from '../services/AxiosApi';
import {errorRequest} from '../utils/fnUtils';

type AuthContextType = {
  user?: IUser;
  setUser(user: IUser): void;
  requestLogin(login: ILogin): void;
  isLoading: boolean;
  logout(): void;
};

type AuthContextProviderProps = {
  children: ReactNode;
};

export const AuthContext = createContext({} as AuthContextType);

export function AuthContextProvider({children}: AuthContextProviderProps) {
  const [user, setUser] = useState<IUser>();
  const [isLoading, setIsLoading] = useState(false);

  async function requestLogin(login: ILogin) {
    setIsLoading(true);

    try {
      const response = await Requests.auth.login(login.email, login.password);
      if (response.data) {
        setBearerToken(response.headers.authorization);

        const newUser = {
          ...response.data,
          token: response.headers.authorization,
        };
        setUser(response.data);

        await AsyncStorage.setItem(KEY_USER_STORAGE, JSON.stringify(newUser));
      } else {
        errorRequest(response.errors?.message);
      }
    } catch (err) {
      const errorResponse: IErrorResponse = err;
      if (errorResponse.response?.data) {
        errorRequest(errorResponse.response.data.errors.message);
      } else {
        errorRequest(errorResponse.message);
      }
    } finally {
      setIsLoading(false);
    }
  }

  async function logout() {
    setUser(undefined);
    await AsyncStorage.removeItem(KEY_USER_STORAGE);
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        requestLogin,
        isLoading,
        logout,
      }}>
      {children}
    </AuthContext.Provider>
  );
}
