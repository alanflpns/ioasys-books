import React, {createContext, ReactNode, useState} from 'react';
import {useNavigation} from '@react-navigation/native';

import {IErrorResponse} from '../interfaces/system';
import Requests from '../services/Requests';
import {IBook} from '../interfaces/book';
import {errorRequest} from '../utils/fnUtils';
import {useAuth} from '../hooks/useAuth';

export interface IFilters {
  title?: string;
  published?: number[];
  category?: string[];
}

type BookContextType = {
  bookList?: IBook[];
  getBookList(page?: number, filters?: IFilters): void;
  getMoreBooks(): Promise<null | undefined>;
  totalBooks?: number;
  book?: IBook;
  getInfoBook(bookId: string): void;
  isLoadingBookList: boolean;
  isLoadingBook: boolean;
};

type BookContextProviderProps = {
  children: ReactNode;
};

export const BookContext = createContext({} as BookContextType);

export function BookContextProvider({children}: BookContextProviderProps) {
  const {logout} = useAuth();
  const navigation = useNavigation();

  const [bookList, setBookList] = useState<IBook[]>();
  const [isLoadingBookList, setIsLoadingBookList] = useState(false);
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [, setTotalPages] = useState<number>();
  const [totalBooks, setTotalBooks] = useState<number>();

  const [book, setBook] = useState<IBook>();
  const [isLoadingBook, setIsLoadingBook] = useState(false);
  const [filtersBook, setFiltersBook] = useState<IFilters>();

  async function getBookList(page?: number, filters?: IFilters) {
    const newPage = page || currentPage;

    setIsLoadingBookList(true);
    setFiltersBook(filters);

    try {
      const response = await Requests.books.getBookList(newPage, filters);
      if (response.data) {
        setBookList(response.data.data);
        setCurrentPage(newPage + 1);
        setTotalPages(response.data.totalPages);
        setTotalBooks(response.data.totalItems);
      } else {
        errorRequest(response.errors?.message);
      }
    } catch (err) {
      const errorResponse: IErrorResponse = err;
      if (
        errorResponse.response?.data &&
        errorResponse.response?.data.errors.message === 'Não autorizado.'
      ) {
        logout();
        errorRequest('Você foi deslogado. Por favor entre novamente!');
      } else if (errorResponse.response?.data) {
        errorRequest(errorResponse.response.data.errors.message);
      } else {
        errorRequest(errorResponse.message);
      }
    } finally {
      setIsLoadingBookList(false);
    }
  }

  async function getMoreBooks() {
    if ((bookList && bookList?.length === totalBooks) || isLoadingMore)
      return null;

    setIsLoadingMore(true);

    try {
      const response = await Requests.books.getBookList(
        currentPage,
        filtersBook,
      );
      if (response.data) {
        setBookList([...bookList!, ...response.data.data]);
        setCurrentPage(currentPage + 1);
      } else {
        errorRequest(response.errors?.message);
      }
    } catch (err) {
      const errorResponse: IErrorResponse = err;
      if (
        errorResponse.response?.data &&
        errorResponse.response?.data.errors.message === 'Não autorizado.'
      ) {
        logout();
        errorRequest('Você foi deslogado. Por favor entre novamente!');
      } else if (errorResponse.response?.data) {
        errorRequest(errorResponse.response.data.errors.message);
      } else {
        errorRequest(errorResponse.message);
      }
    } finally {
      setIsLoadingMore(false);
    }
  }

  async function getInfoBook(bookId: string) {
    navigation.navigate('BookInfo' as never);

    setIsLoadingBook(true);

    try {
      const response = await Requests.books.getBookById(bookId);
      if (response.data) {
        setBook(response.data);
      } else {
        errorRequest(response.errors?.message);
        navigation.goBack();
      }
    } catch (err) {
      const errorResponse: IErrorResponse = err;
      if (
        errorResponse.response?.data &&
        errorResponse.response?.data.errors.message === 'Não autorizado.'
      ) {
        logout();
        errorRequest('Você foi deslogado. Por favor entre novamente!');
      } else if (errorResponse.response?.data) {
        errorRequest(errorResponse.response.data.errors.message);
      } else {
        errorRequest(errorResponse.message);
      }
    } finally {
      setIsLoadingBook(false);
    }
  }

  return (
    <BookContext.Provider
      value={{
        bookList,
        getBookList,
        getMoreBooks,
        totalBooks,
        book,
        getInfoBook,
        isLoadingBookList,
        isLoadingBook,
      }}>
      {children}
    </BookContext.Provider>
  );
}
