import React, {useCallback} from 'react';
import {ActivityIndicator, ListRenderItem, RefreshControl} from 'react-native';
import theme from '../../global/styles/theme';

import {useBook} from '../../hooks/useBook';
import {BooksList, NoBooksText} from './styles';
import {Card} from '../../components/card/Card';
import {IBook} from '../../interfaces/book';

export function BookList() {
  const {bookList, getMoreBooks, getInfoBook, totalBooks, getBookList} =
    useBook();

  const renderItems: ListRenderItem<IBook> = useCallback(
    ({item}) => (
      <Card
        image={item.imageUrl}
        title={item.title}
        authors={item.authors}
        pages={item.pageCount}
        company={item.publisher}
        publication={item.published}
        onPress={() => getInfoBook(item.id)}
      />
    ),
    [],
  );

  if (bookList?.length === 0)
    return <NoBooksText>Nenhum livro encontrado</NoBooksText>;

  return (
    <BooksList
      data={bookList}
      keyExtractor={item => item.id}
      initialNumToRender={10}
      renderItem={renderItems}
      onEndReached={() => getMoreBooks()}
      ListFooterComponent={
        bookList && bookList?.length !== totalBooks ? (
          <ActivityIndicator color={theme.colors.pink800} size="large" />
        ) : null
      }
      onEndReachedThreshold={0.3}
      refreshControl={
        <RefreshControl
          refreshing={false}
          tintColor={theme.colors.pink800}
          onRefresh={() => getBookList(1)}
        />
      }
    />
  );
}
