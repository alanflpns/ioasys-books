/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-return-assign */
import queryString from 'query-string';
import {Alert} from 'react-native';

export const stringifyQueryStringArray = (obj: any) => {
  const keys = Object.keys(obj).filter(
    key => obj[key] !== '' && obj[key] !== ' ',
  );
  const retorno = {};
  keys.forEach(key => ((retorno as any)[key] = obj[key]));
  return queryString.stringify(retorno, {arrayFormat: 'bracket'});
};

export const errorRequest = (error?: string) =>
  error ? Alert.alert('Ops!', error) : null;
