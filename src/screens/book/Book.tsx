import React from 'react';
import {StatusBar, ScrollView, ActivityIndicator} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {
  ContainerHeader,
  BackScreen,
  Icon,
  Container,
  ContainerLoading,
  BookImage,
  Title,
  Authors,
  TitleInfo,
  InfoContainer,
  Info,
  Label,
  Value,
  TitleResenha,
  Resenha,
} from './styles';
import {useBook} from '../../hooks/useBook';
import theme from '../../global/styles/theme';

export function Book() {
  const {isLoadingBook, book} = useBook();
  const navigation = useNavigation();

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />

      <ContainerHeader>
        <BackScreen activeOpacity={0.5} onPress={() => navigation.goBack()}>
          <Icon name="arrow-back" />
        </BackScreen>
      </ContainerHeader>

      {isLoadingBook ? (
        <ContainerLoading>
          <ActivityIndicator size={50} color={theme.colors.pink800} />
        </ContainerLoading>
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <Container>
            <BookImage source={{uri: book?.imageUrl}} />

            <Title>{book?.title}</Title>
            <Authors>
              {book?.authors.map(
                (author, index, array) =>
                  `${author}${index !== array.length - 1 ? ', ' : ''}`,
              )}
            </Authors>

            <TitleInfo>INFORMAÇÕES</TitleInfo>
            <InfoContainer>
              <Info>
                <Label>Páginas</Label>
                <Value>{book?.pageCount} páginas</Value>
              </Info>
              <Info>
                <Label>Editora</Label>
                <Value>{book?.publisher}</Value>
              </Info>
              <Info>
                <Label>Publicação</Label>
                <Value>{book?.published}</Value>
              </Info>
              <Info>
                <Label>Idioma</Label>
                <Value>{book?.language}</Value>
              </Info>
              <Info>
                <Label>ISBN-10</Label>
                <Value>{book?.isbn10}</Value>
              </Info>
              <Info>
                <Label>ISBN-13</Label>
                <Value>{book?.isbn13}</Value>
              </Info>
              <Info>
                <Label>Categoria</Label>
                <Value>{book?.category}</Value>
              </Info>
            </InfoContainer>

            <TitleResenha>RESENHA DA EDITORA</TitleResenha>
            <Resenha>{book?.description}</Resenha>
          </Container>
        </ScrollView>
      )}
    </>
  );
}
