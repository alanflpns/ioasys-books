import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Dashboard} from '../screens/dashboard/Dashboard';
import theme from '../global/styles/theme';
import {Book} from '../screens/book/Book';

export type RootStackParamList = {
  Dashboard: undefined;
  BookInfo: undefined;
};

export function AppNavigation() {
  const App = createNativeStackNavigator<RootStackParamList>();

  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Dashboard">
      <App.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          headerStyle: {
            backgroundColor: theme.colors.silver500,
          },
        }}
      />
      <App.Screen
        name="BookInfo"
        component={Book}
        options={{
          headerStyle: {
            backgroundColor: theme.colors.white,
          },
        }}
      />
    </App.Navigator>
  );
}
