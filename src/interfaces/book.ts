export interface IBook {
  id: string;
  title: string;
  description: string;
  authors: string[];
  pageCount: number;
  category: string;
  imageUrl: string;
  isbn10: string;
  isbn13: string;
  language: string;
  publisher: string;
  published: number;
}

export interface IBookResponseRequest {
  data: IBook[];
  page: number;
  totalPages: number;
  totalItems: number;
}
