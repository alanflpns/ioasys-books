import React, {memo} from 'react';
import {View, TouchableOpacityProps} from 'react-native';

import {
  Container,
  BookContainer,
  ImageBook,
  InfoContainer,
  Title,
  Author,
  Pages,
  Company,
  Publication,
} from './styles';

export interface ICardProps extends TouchableOpacityProps {
  image: string;
  title: string;
  authors: string[];
  pages: number;
  company: string;
  publication: number;
}

function CardComponent({
  image,
  title,
  authors,
  pages,
  company,
  publication,
  ...rest
}: ICardProps) {
  return (
    <Container activeOpacity={0.6} {...rest}>
      <BookContainer>
        <ImageBook source={{uri: image}} />

        <InfoContainer>
          <View>
            <Title>{title}</Title>
            <Author>
              {authors.map(
                (author, index, array) =>
                  `${author}${index !== array.length - 1 ? ', ' : ''}`,
              )}
            </Author>
          </View>

          <View>
            <Pages>{pages} páginas</Pages>
            <Company>{company}</Company>
            <Publication>Publicado em {publication}</Publication>
          </View>
        </InfoContainer>
      </BookContainer>
    </Container>
  );
}

export const Card = memo(CardComponent, (prevProps, nextProps) =>
  Object.is(prevProps, nextProps),
);
