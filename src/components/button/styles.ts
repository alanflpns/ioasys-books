import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface ITypeProps {
  type: 'light' | 'black' | 'default';
}
interface ITextProps extends ITypeProps {
  isLoading?: boolean;
}

export const Container = styled.TouchableOpacity<ITypeProps>`
  background-color: ${({theme, type}) =>
    type === 'black' ? theme.colors.black : theme.colors.white};

  padding: 8px 20px;
  border-radius: 45px;

  position: relative;
  border-width: 1px;
  border-color: ${({theme}) => theme.colors.white};

  ${({type}) =>
    type === 'light' &&
    css`
      border-width: 1px;
      border-color: ${({theme}) => theme.colors.black};
    `}
`;

export const TextButton = styled.Text<ITextProps>`
  color: ${({theme, type}) =>
    type === 'black'
      ? theme.colors.white
      : type === 'light'
      ? theme.colors.black
      : theme.colors.pink800};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;

  ${({isLoading}) =>
    isLoading &&
    css`
      opacity: 0;
    `}
`;

export const LoadingContainer = styled.View`
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
`;
